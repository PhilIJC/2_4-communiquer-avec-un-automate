- [Présentation](#présentation)
- [Quelques notions sur le protocole modbus](#quelques-notions-sur-le-protocole-modbus)
  - [Format des données envoyées par le maitre](#format-des-données-envoyées-par-le-maitre)
  - [Format des données envoyées par l'esclave](#format-des-données-envoyées-par-lesclave)
  - [Les différents types de données (registres)](#les-différents-types-de-données-registres)
- [Implémentation du protocole modbus dans EPICS](#implémentation-du-protocole-modbus-dans-epics)
  - [communications supportées](#communications-supportées)
  - [librairies support](#librairies-support)
  - [Les types de données supportés](#les-types-de-données-supportés)
  - [Les function code modbus supportés](#les-function-code-modbus-supportés)
  - [adressage modbus, taille des données](#adressage-modbus-taille-des-données)
  - [Les grandes étapes pour implémenter une communication modbus TCP/IP](#les-grandes-étapes-pour-implémenter-une-communication-modbus-tcpip)
    - [Créer une connection TCP](#créer-une-connection-tcp)
    - [Configurer le timeout pour les opérations de lecture/écriture](#configurer-le-timeout-pour-les-opérations-de-lectureécriture)
    - [configurer la communication modbus](#configurer-la-communication-modbus)
    - [Relier chaque record à une adresse modbus](#relier-chaque-record-à-une-adresse-modbus)
- [Quelques notions sur le protocole S7-PLC](#quelques-notions-sur-le-protocole-s7-plc)
  - [Le driver EPICS pour S7-PLC](#le-driver-epics-pour-s7-plc)
  - [Quelques exemples de record configurés pour S7-PLC](#quelques-exemples-de-record-configurés-pour-s7-plc)
- [Quelques notions sur le protocole OPC-UA](#quelques-notions-sur-le-protocole-opc-ua)
# Présentation

Les automates industriels programmables (API en Anglais) sont très utilisés dans le monde des accélérateurs de particules. Afin de pouvoir les contrôler depuis des stations (linux, windows), il faut les connecter à un réseau et utiliser un protocole de communication dédié.

Une des normes les plus utilisés dans l'industrie des automates est le protocole modbus.
- Il a été créé en 1979 par Modicon (racheté en 1979 par Scheider Electric)
- Il est dans le domaine public donc gratuit
- Il existe en 2 modes :
  - mode RTU (RS232, RS422, RS485) pour les liaisons série
  - mode TCP pour les liaisons ethernet et wifi (Modbus over TCP/IP)

Cf https://modbus.org/docs/Modbus_Application_Protocol_V1_1b.pdf pour la spec complète.

![Alt text](MVI69E-MBTCP.png)

EPICS fournit des logiciels qui permettent de s'interfacer avec des équipements en modbus.

# Quelques notions sur le protocole modbus

C'est un protocole basé sur une architecture client/serveur ou esclave/maitre.
- un maitre envoie des requête sur le réseau et les esclaves répondent
- les esclaves n'envoient jamais rien de leur propre initiative. Ils ne font que répondre
- Chaque esclave a une adresse IP avec un numéro de port (en général 502) (par exemple 172.16.10.255:502)
- Chaque esclave a un numéro unique sur le réseau (de 1 à 65535)

![Alt text](modbusMasterSlave.png)

## Format des données envoyées par le maitre

Pour le protocole modbus TCP, les données transitent sur un réseau ethernet et sont encapsulées sous la forme suivante :

![Alt text](modbusMasterDataFormat.png)

- Slave Address (16 bits) : adress de l'esclave.
- function code (8 bits) : code de la fonction modbus

![Alt text](modbusFunctionCode.png)

- Data : les données à écrire si c'est une function code d'écriture
- CRC : un code d'erreur permettant de vérifier que les données sont correctes (n'ont pas été altéré sur le réseau)

## Format des données envoyées par l'esclave

L'esclave répond au maitre en envoyant des données sous la forme suivante :

![Alt text](modbusSlaveDataFormat.png)

- Request : identifiant de la requête du maitre (le function code)
- data : les données à renvoyer au maitre
- CRC : un code d'erreur permettant de vérifier que les données sont correctes (n'ont pas été altéré sur le réseau)


## Les différents types de données (registres)
Dans chaque équipement (par exemple un automate industriel), il existe une mémoire qui contient les données modbus (registres). Ces données peuvent êtres des valeurs d'entrée (input) ou de sortie (output).

![Alt text](modbusMemory.png)

Ces données sont organisées en registres de 16 bits.

![Alt text](modbusRegisters.png)
- Coils outputs : données pour sortir des valeurs binaires (0 ou 1). Commander un contacteur par exemple.
- Discrete inputs : données pour lire des valeurs binaires (0 ou 1). Lire un contacteur ON/OFF par exemple.
- Input Registers : données pour lire des valeurs analogiques
- Holding Registers : données pour écrire et lire des valeurs analogiques

Pour chaque function code correspond un registre particulier. 

**Exemple 1 :** Si le maitre veut écrire la valeur décimale 16 (codé sur 1 mot de 16 bits) sur l'esclave 1 dans le "holding" registre 4000 alors il enverra la trame suivante :
```shell
|slave address   |FuncCode|                  Data                    |   CRC     |
|                |        |start address|offset |length|data to write|           |
|      1         |    6   |   4000       |   0  |  1   |     16      |xxxxxxxxxxx|
```

- L'offset est un offset par rapport à l'addresse de départ (start address).
- length indique la taille des données en mots (16 bits) à transmettre.

En retour, l'esclave renverra la trame suivante :
```shell
|Request |                        Data                |   CRC     |
|    6   |                                            |xxxxxxxxxxx|
```


**Exemple 2 :** Si le maitre veut lire 10 valeurs binaire sur l'esclave 2 à partir de l'adresse 6000 où se trouvent des registres "discrete input" alors il enverra la trame suivante :
```shell
|slave address   |FuncCode|Data                                        |   CRC     |
|                |        |start address|offset |length                |           |
|      2         |    2   |   6000       |   0  |  10                  |xxxxxxxxxxx|
```

En retour, l'esclave renverra la trame suivante :
```shell
|Request |                        Data                |   CRC     |
|length  |length|                read data            |           |
|    2   |10    |1001100110                           |xxxxxxxxxxx|
```
# Implémentation du protocole modbus dans EPICS 

## communications supportées
EPICS supporte les types de communication modbus suivants :
- TCP
- RTU (liaison série RS-232, RS-422, or RS-485)
- serial ASCII (liaison série RS-232, RS-422, or RS-485) : moins rapide que RTU mais plus fiable

cf https://cars9.uchicago.edu/software/epics/Modbus_Application_Protocol_V1_1b.pdf pour la doc complète modbus EPICS.

## librairies support

EPICS fournit des librairies "support" qui permettent de communiquer avec des équipements utilisant le protocole modbus.

![Alt text](epicsAsyn.png)

- modbus : librairie qui gère le protocole modbus
- streamDevice (librairie qui gère le protocole "byte stream". C'est un protocole dans lequel les équipemements (devices) peuvent être contrôlés en échangeant des données de type string. Ce sont typiquement les communications série (RS-232, RS-485), IEEE-488 (aussi appelé GPIB), les communications TCP/IP.)
-  asyn (librairie qui permet d'interfacer le code bas niveau des équipements). Les librairies asyn et stream sont complémentaires. stream convertit les flux de données à échanger en "stream" et asyn s'occupe de la communication avec l'équipement (device).

## Les types de données supportés
EPICS supporte les données de type :
| Primary tables | Object type | Access 3 | Comments |
|-----------|-----------|-----------|-----------|  	
|Discrete Inputs| 	Single bit| 	Read-Only| 	This type of data can be provided by an I/O system.|
|Coils| 	Single bit| 	Read-Write| 	This type of data can be alterable by an application program.|
|Input Registers| 	16-bit word| 	Read-Only| 	This type of data can be provided by an I/O system.|
|Holding Registers| 	16-bit word| 	Read-Write| 	This type of data can be alterable by an application program. |

Dans le cas des automates, on utilise en général uniquement les Holding Registers. (en tous cas au GANIL).

## Les function code modbus supportés

| Access | Function description | Function code 3 |
|-----------|-----------|-----------|	
|Bit access| 	Read Coils| 	1|
|Bit access| 	Read Discrete Inputs| 	2|
|Bit access| 	Write Single Coil 	|5|
|Bit access| 	Write Multiple Coils |	15|
|16-bit word access |	Read Input Registers |	4|
|16-bit word access |	Read Holding Registers| 	3|
|16-bit word access |	Write Single Register |	6|
|16-bit word access |	Write Multiple Registers |	16|
|16-bit word access |	Read/Write Multiple Registers |	23|

## adressage modbus, taille des données

- Les adresses des registres sont codés sur un mot de 16 bits.
- en lecture, la taille maximale des données à lire est de 125 mots de 16 bits soit 2000 bits.
- en écriture, la taille maximale des données à lire est de 123 mots de 16 bits soit 1968 bits.

## Les grandes étapes pour implémenter une communication modbus TCP/IP

### Créer une connection TCP
```makefile
drvAsynIPPortConfigure("portName","hostInfo",priority,noAutoConnect,noProcessEos)
Exemple : drvAsynIPPortConfigure("ALIM_TCP_PORT","164.54.160.158:502",0,0,1)
```

| parameter | Data type | Description |
|-----------|-----------|-----------|	
|portName   |string |The portName that is registered with asynManager.|
|priority|int| Priority at which the asyn I/O thread will run. If this is zero or missing, then epicsThreadPriorityMedium is used.|
|noAutoConnect|int|Zero or missing indicates that portThread should automatically connect. Non-zero if explicit connect command must be issued.|
|noProcessEos|int| If 0 then asynInterposeEosConfig is called specifying both processEosIn and processEosOut.|

___
**NOTE :**
C'est cette function qui crée le driver asyn.
___

### Configurer le timeout pour les opérations de lecture/écriture

On va configurer le timeout pour une opération d'écriture sur le port TCP créé précédemment.
```makefile
modbusInterposeConfig(tcpPortName, 
                       linkType (0 for TCP),
                       timeoutMsec,
                       writeDelayMsec)
Exemple : modbusInterposeConfig("ALIM_TCP_PORT",0,300,2)
```

| parameter | Data type | Description |
|-----------|-----------|-----------|	
|portName| 	string| 	Name of the asynIPPort or asynSerialPort previously created.|
|linkType| 	int| 	Modbus link layer type: 0 = TCP/IP<br>1 = RTU<br>2 = ASCII|
|timeoutMsec| 	int| 	The timeout in milliseconds for write and read operations to the underlying asynOctet driver. This value is used in place of the timeout parameter specified in EPICS device support. If zero is specified then a default timeout of 2000 milliseconds is used.|
|writeDelayMsec| 	int| 	The delay in milliseconds before each write from EPICS to the device. This is typically only needed for Serial RTU devices. The Modicon Modbus Protocol Reference Guide says this must be at least 3.5 character times, e.g. about 3.5ms at 9600 baud, for Serial RTU. The default is 0. |


**NOTE :**
C'est cette function qui crée le driver "interpose interface".

### configurer la communication modbus
La fonction suivante va permettre de configurer :
- l'adresse de l'esclave (slaveAddress)
- la function modbus (modbusFunction)
- l'adresse mémoire du registre à lire ou écrire (modbusStartAddress)
- la longueur des données à lire ou écrire (modbusLength)
- le type de données (dataType) : entier 16 bits, entier 32 bits, flottant ...
- la valeur du polling en ms pour les opérations de lecture (pollMsec)
- le type d'automate (plcType)

Les types de données supportées sont les suivants :
| modbusDataType value | drvUser field | Description |
|-----------|-----------|-----------|	
|0 |	UINT16 	|Unsigned 16-bit binary integers|
|1 |	INT16SM |	16-bit binary integers, sign and magnitude format. In this format bit 15 is the sign bit, and bits 0-14 are the absolute value of the magnitude of the number. This is one of the formats used, for example, by Koyo PLCs for numbers such as ADC conversions.|
|2 |	BCD_UNSIGNED |	Binary coded decimal (BCD), unsigned. This data type is for a 16-bit number consisting of 4 4-bit nibbles, each of which encodes a decimal number from 0-9. A BCD number can thus store numbers from 0 to 9999. Many PLCs store some numbers in BCD format.|
|3 |	BCD_SIGNED| 	4-digit binary coded decimal (BCD), signed. This data type is for a 16-bit number consisting of 3 4-bit nibbles, and one 3-bit nibble. Bit 15 is a sign bit. Signed BCD numbers can hold values from -7999 to +7999. This is one of the formats used by Koyo PLCs for numbers such as ADC conversions.|
|4 |	INT16| 	16-bit signed (2's complement) integers. This data type extends the sign bit when converting to epicsInt32.
|5 |	INT32_LE |	32-bit integers, little endian (least significant word at Modbus address N, most significant word at Modbus address N+1)|
|6 	|INT32_BE| 	32-bit integers, big endian (most significant word at Modbus address N, least significant word at Modbus address N+1)|
|7 |	FLOAT32_LE |	32-bit floating point, little endian (least significant word at Modbus address N, most significant word at Modbus address N+1)|
|8| 	FLOAT32_BE |	32-bit floating point, big endian (most significant word at Modbus address N, least significant word at Modbus address N+1)|
|9 	|FLOAT64_LE |	64-bit floating point, little endian (least significant word at Modbus address N, most significant word at Modbus address N+3)|
|10 |	FLOAT64_BE |	64-bit floating point, big endian (most significant word at Modbus address N, least significant word at Modbus address N+3)|
|11 |	STRING_HIGH| 	String data. One character is stored in the high byte of each register.|
|12 |	STRING_LOW| 	String data. One character is stored in the low byte of each register.|
|11 |	STRING_HIGH_LOW |	String data. Two characters are stored in each register, the first in the high byte and the second in the low byte.|
|11| 	STRING_LOW_HIGH |	String data. Two characters are stored in each register, the first in the low byte and the second in the high byte. |

```shell
drvModbusAsynConfigure(portName, 
                       tcpPortName,
                       slaveAddress, 
                       modbusFunction, 
                       modbusStartAddress, 
                       modbusLength,
                       dataType,
                       pollMsec, 
                       plcType);
Exemple : drvModbusAsynConfigure("ALIM_READ_REG0_L2_PORT","ALIM_TCP_PORT",1, 3, 0, 2, 0, 500,"PLC")
```
### Relier chaque record à une adresse modbus

Dans le fichier de définition des records, on va relier chaque record à une configuration modbus définie précédemment. Le deux informations importantes sont :
- DTYP : le type de données à échanger
  - asynUInt32Digital : mots de 32 bits non signés
  - asynInt32 : mots de 32 bits signés
  - asynInt32Array : tableau de mots de 32 bits signés (jusqu'à 125 registres de 16 bits)
  - asynFloat64 : flottant sur 64 bits
  - asynOctet : strings jusqu'à 250 caractères
- INP ou OUT : On utilise ce champ avec la fonction `asyn` pour relier le record à une configuration modbus
```c
field(INP,"@asynMask(portName,offset,nbits,timeout)drvUser")
```

Exemple : 
```c
record(ai, "$(user):ValCourant") {
        field(SCAN, "I/O Intr")
        field(DESC, "Valeur du courant")
        field(DTYP, "asynInt32")
        field(INP, "@asyn($(VAL_COURANT_PORT) $(VAL_COURANT_OFFSET) 2000)INT32_BE")
        field(PREC,  "3")
        field(EGU,  "A")
}
```

**NOTE :**
- VAL_COURANT_PORT contient le port configuré avec la fonction `drvModbusAsynConfigure`
- VAL_COURANT_OFFSET contient la valeur de l'offset depuis l'addresse modbus d'un registre
- 2000 est la valeur en ms du timeout pour les opérations de lecture/écriture. Elle écrase la valeur définie précédemment avec la fonction `modbusInterposeConfig`.

# Quelques notions sur le protocole S7-PLC

## Le driver EPICS pour S7-PLC
La sérié S7 des automates Siemens propose un protocole de communication dédié. Il existe un driver EPICS qui permet de faire communiquer un IOC EPICS via TCP/IP avec un automate Siemens série S7.

Cf http://epics.web.psi.ch/style/software/s7plc/s7plc.html pour avoir tout le détail sur ce driver EPICS.

On va retrouver des fonction de configuration comme pour le modbus :
```c
s7plcConfigure (PLCname, IPaddr, port, inSize, outSize, bigEndian, recvTimeout, sendIntervall)
```
Exemple : 
```c
s7plcConfigure ("vak-4", "192.168.0.10", 2000, 1024, 32, 1, 500, 100)
```

## Quelques exemples de record configurés pour S7-PLC
```c
  record (ai, "$(RECORDNAME)") {
    field (DTYP, "S7plc")
    field (INP,  "@$(PLCNAME)/$(OFFSET) T=UINT16 L=0 H=20")
    field (SCAN, "I/O Intr")
    field (LINR, "Linear")
    field (EGUL, "$(MINVAL)")
    field (EGUF, "$(MAXVAL)")
  }

record(bi, "$(NAME)") {
  field (DTYP, "S7plc")
  field (INP,  "@$(PLCNAME)/$(OFFSET) T=$(T) B=$(B)")
  field (SCAN, "I/O Intr")
}

record(bo, "$(NAME)") {
  field (DTYP, "S7plc")
  field (OUT,  "@$(PLCNAME)/$(OFFSET) T=$(T) B=bit")
  field (PINI, "YES")
}
```

**NOTE :**
- OFFSET est l'offset en mots de 8 bits (byte) dans le databloc que l'on veut addresser sur l'automate. C'est en quelque-sorte l'adresse du registre automate.
- T est le type de données (INT8, INT16, INT32, UINT8, UINT16, UINT32, REAL32, REAL64, STRING)
- uniquement pour les records AI et AO :
  - L est la valeur minimimum que peut prendre la PV
  - H est la valeur maximum que peut prendre la PV
  
# Quelques notions sur le protocole OPC-UA

- La norme **OPC UA CEI 62541** fournit une plateforme indépendante qui permet un échange d'informations **sécurisé et fiable**. 
- La norme OPC UA prend en charge les services et protocoles **client/serveur** et les modèles et protocoles de **publication/abonnement (PubSub)**. OPC UA peut fonctionner sur une relation client/serveur dédiée. Dans le scénario PubSub, le serveur envoie (publie) des données sur le réseau, et le client (qui s'est abonné) recevra les données. Il est important de noter que dans la spécification OPC UA, l'authentification, la signature et le cryptage des données sont fortement soulignés pour les modèles client/serveur et PubSub.
- Ce protocole est ouvert pour que tout le monde puisse l'utiliser et le mettre en œuvre sous la **licence GPL 2.0**.
- Il est multiplateforme : elle n'est pas liée à un seul système d'exploitation ou langage de programmation.
Sécurité accrue du protocole : qui permet aux utilisateurs d'accéder à l'authentification, l'autorisation, l'intégrité et la confidentialité.

Un driver EPICS a été développé pour supporter ce protocole. cf https://github.com/epics-modules/opcua.

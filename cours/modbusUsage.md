# lancer un esclave modbus TCP
```shell
sudo diagslave -m tcp
diagslave 3.4 - FieldTalk(tm) Modbus(R) Diagnostic Slave Simulator
Copyright (c) 2002-2021 proconX Pty Ltd
Visit https://www.modbusdriver.com for Modbus libraries and tools.

Protocol configuration: MODBUS/TCP
Slave configuration: address = -1, master activity t/o = 3.00s
IP configuration: port = 502, connection t/o = 60.00s

Server started up successfully.
Listening to network (Ctrl-C to stop)
```
___
**Note :**
- il faut être root pour pouvoir ouvrir une connection TCP sous linux d'où la commande `sudo`.
___

# Envoyer des commandes modbus avec modpoll (master)
```shell
Usage: modpoll [OPTIONS] SERIALPORT|HOST [WRITEVALUES...]
Arguments:
SERIALPORT    Serial port when using Modbus ASCII or Modbus RTU protocol
              COM1, COM2 ...                on Windows
              /dev/ttyS0, /dev/ttyS1 ...    on Linux
HOST          Host name or dotted IP address when using MODBUS/TCP protocol
General options:
-m ascii      Modbus ASCII protocol
-m rtu        Modbus RTU protocol (default if SERIALPORT contains /, \\ or COM)
-m tcp        MODBUS/TCP protocol (default otherwise)
-m udp        MODBUS UDP
-m enc        Encapsulated Modbus RTU over TCP
-a #          Slave address (1-255 for serial, 0-255 for TCP, 1 is default)\n
-r #          Start reference (1-65536, 1 is default)
-c #          Number of values to read (1-125, 1 is default), optional for writ
ing (use -c 1 to force FC5 or FC6)
-t 0          Discrete output (coil) data type
-t 1          Discrete input data type
-t 3          16-bit input register data type
-t 3:hex      16-bit input register data type with hex display
-t 3:int      32-bit integer data type in input register table
-t 3:mod      32-bit module 10000 data type in input register table
-t 3:float    32-bit float data type in input register table
-t 4          16-bit output (holding) register data type (default)
-t 4:hex      16-bit output (holding) register data type with hex display
-t 4:int      32-bit integer data type in output (holding) register table
-t 4:mod      32-bit module 10000 type in output (holding) register table
-t 4:float    32-bit float data type in output (holding) register table
-i            Slave operates on big-endian 32-bit integers
-f            Slave operates on big-endian 32-bit floats
-e            Use Daniel/Enron single register 32-bit mode
-0            First reference is 0 (PDU addressing) instead 1
-1            Poll only once only, otherwise every poll rate interval
-l            Poll rate in ms, (1000 is default)
-o #          Time-out in seconds (0.01 - 10.0, 1.0 s is default)
Options for MODBUS/TCP, UDP and RTU over TCP:
-p #          IP protocol port number (502 is default)
Options for Modbus ASCII and Modbus RTU:
-b #          Baudrate (e.g. 9600, 19200, ...) (19200 is default)
-d #          Databits (7 or 8 for ASCII protocol, 8 for RTU)
-s #          Stopbits (1 or 2, 1 is default)
-p none       No parity
-p even       Even parity (default)
-p odd        Odd parity
-4 #          RS-485 mode, RTS on while transmitting and another # ms after
```

On utilise l'adresse IP 127.0.0.1 et le port 502 (-p 502) qui est également le port par défaut

- envoyer la valeur 1234 à l'esclave 1 (-a 2) dans le holding register (registrer d'écriture de l'esclave) (-t 4) qui se situe à l'adresse 1201 (-r 1201)  
```shell
modpoll -a 1 -t 0 -r 1201 -p 502 127.0.0.1 0
```

- envoyer la valeur 1 à l'esclave 2 (-a 2) dans le coil register (registres booléens de l'esclave) (-t 0) qui se situe à l'adresse 1202 (-r 1201)  
```shell
modpoll -a 2 -t 0 -r 1202 -p 502 127.0.0.1 1
```

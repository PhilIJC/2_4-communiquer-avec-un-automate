# Informations

## Objectifs

- Créer une application permettant de communiquer par protocole modbus avec un automate
- explication de la structure et des commandes caget, caput (sans détail)

## Durée : 60 min

## Prérequis

- savoir créer un IOC
- connaitre les fonctions de base des records EPICS

## Déroulement

1. Configuration de l'IOC
2. Création d'une application d'exemple
3. Communication avec un équipement MODBUS/TCP

# Ajout des librairies support qui vont permettre de communiquer avec un équipement modbus

## Modification du make file
Dans le fichier `modbusExampleApp/src/Makefile`, on va ajouter les lignes  suivantes afin d'informer notre application qu'il faut utiliser les librairies suivantes :
- stream (librairie qui gère le protocole "byte stream". C'est un protocole dans lequel les équipemements (devices) peuvent être contrôlés en échangeant des données de type string. Ce sont typiquement les communications série (RS-232, RS-485), IEEE-488 (aussi appelé GPIB), les communications TCP/IP.)
-  asyn (librairie qui permet d'interfacer le code bas niveau des équipements). Les librairies asyn et stream sont complémentaires. stream convertit les flux de données à échanger en "stream" et asyn s'occupe de la communication avec l'équipement (device).
- calc (librairie de calcul qui est utilisée par la librairie stream)
- modbus (librairie qui gère le protocole modbus)

```makefile
# Finally link IOC to the EPICS Base libraries
modbusExample_LIBS += $(EPICS_BASE_IOC_LIBS)

#add calc asyn and streamDevice to this IOC production libs
modbusExample_LIBS += calc
modbusExample_LIBS += stream
modbusExample_LIBS += asyn
modbusExample_LIBS += modbus
```

## Chargement des fichiers calc.dbd, stream.dbd et asyn.dbd
Dans le fichier `modbusExampleApp/src/xxxSupport.dbd`, on va ajouter les lignes suivantes :
```c
# ajout support calc, stream, asyn, modbus
include "calc.dbd"
include "stream.dbd"
include "asyn.dbd"
include "modbus.dbd"
include "modbusSupport.dbd"
registrar(drvAsynIPPortRegisterCommands)
registrar(drvAsynSerialPortRegisterCommands)
registrar(vxi11RegisterCommands)
registrar(drvModbusAsynRegister)
registrar(modbusInterposeRegister)
```

**NOTE :**
- le mot clé `registar` permet d'indiquer au compilateur qu'on va utiliser une fonction donnée (drvAsynIPPortRegisterCommands, drvModbusAsynRegister ...)
 
## Modification du fichier `configure/RELEASE`
Dans le fichier modbusExampleApp/configure/RELEASE, il faut indiquer le chemin des librairies calc, stream, asyn et modbus.
```makefile
# Variables and paths to dependent modules:
#MODULES = /path/to/modules
#MYMODULE = $(MODULES)/my-module
HOME_EPICS=/opt/epics
SUPPORT=$(HOME_EPICS)/support
CALC = $(SUPPORT)/calc-R3-7-4
ASYN=$(SUPPORT)/asyn-R4-42
STREAM=$(SUPPORT)/StreamDevice-2-8-16
MODBUS=$(SUPPORT)/modbus-R3-2
```

Il faut également déplacer les 2 lignes suivantes à la fin du fichier :
```makefile
# EPICS_BASE should appear last so earlier modules can override stuff:
EPICS_BASE = /opt/epics/base-7.0.6.1
```

## Recompilation de l'IOC
C'est nécessaire pour vérifier que l'IOC compile toujours.

```shell
cd topAutomateModbus
make distclean
make
```

# Utilisation des fonctions EPICS modbus (ASYN)

Nous allons modifier le fichier `topAutomateModbus/iocBoot/iocmodbusExample/st.cmd` afin d'y ajouter les fonctions modbus EPICS.
Toutes les ligne seront à rajouter après la ligne :
```makefile
modbusExample_registerRecordDeviceDriver pdbbase
```

## Création de la connection TCP

On déclare un port "PLC_TCP_PORT" sur une adresse IP locale 127.0.0.1 avec le port 502.
```makefile
# drvAsynIPPortConfigure("portName","hostInfo",priority,noAutoConnect,noProcessEos)
# The default priority is used
# the noAutoConnect flag is set to 0 so that asynManager will do normal automatic connection management. 
# The noProcessEos flag is set to 1 because Modbus over TCP does not require end-of-string processing. 
drvAsynIPPortConfigure("PLC_TCP_PORT","127.0.0.1:502",0,0,1)
```

## Configurer le timeout pour les opérations de lecture/écriture
On configure le port créé précédemment avec un timeout de 300 ms.
Le writeDelayMsec est configuré à 2 mais ne sert pas pour les connections TCP.
```makefile
# modbusInterposeConfig(tcpPortName, 
#                       linkType (0 for TCP),
#                       timeoutMsec,
#                       writeDelayMsec)
modbusInterposeConfig("PLC_TCP_PORT",0,300,2)
```
# Création d'une communication modbus pour lire une valeur analogique sur un automate.

- On va prendre l'exemple d'un automate avec les caractéristiques suivantes :
- l'automate est configuré avec une adresse IP locale 127.0.0.1 et un port qui vaut 502
- l'automate est configuré en tant qu'esclave n°1
- il contient une table de registres avec des adresses allant de 0 à 2000.
- la valeur analogique à lire est une valeur sur 32 bits situé à l'adresse 1000 et utilisant des holding registers.

## Ajout d'un fichier modbus.db pour définir le record EPICS

Ce fichier doit être crée dans modbusExampleApp/Db.
Ici, on va ajouter 1 record qui va lire la valeur du courant d'une alimentation sur un automate.

```c
# record pour lire la valeur du courant
record(ai, "$(user):ValCourant") {
        field(SCAN, "I/O Intr")
        field(DESC, "Valeur du courant")
        field(DTYP, "asynInt32")
        field(INP, "@asyn($(VAL_COURANT_PORT) $(VAL_COURANT_OFFSET) 2000)INT32_BE")
        field(PREC,  "3")
        field(EGU,  "A")
}

```
---
**NOTE :**
- le champ DTYP définit le type de device support à utiliser pour fournir les données à la couche EPICS. Ici on utilise "asnInt32" pour lire des valeurs analogiques sur des équipements.
- le champ INP décrit le lien vers la fonction de communication ainsi que le port et l'adresse à utiliser. Le format de ce champ dépend du protocole utilisé.
- le champ SCAN est configué en mode "I/O Intr" ce qui signifie que le client (maitre) sera notifié des changements de valeur quand la valeur du courant changera réellement sur l'automate.
- Les champs VAL_COURANT_PORT, VAL_COURANT_OFFSET seront définis dans le fichier `topAutomateModbus/iocBoot/iocmodbusExample/st.cmd`
- 2000 est la valeur en ms du timeout pour les opérations de lecture/écriture. Elle écrase la valeur définie précédemment avec la fonction `modbusInterposeConfig`.
---

Il ne faut pas oublier d'ajouter ce fichier dans le fichier modbusExampleApp/Db/Makefile pour pouvoir le compiler.
```makefile
TOP=../..
include $(TOP)/configure/CONFIG
#----------------------------------------
#  ADD MACRO DEFINITIONS BELOW HERE

# Install databases, templates & substitutions like this
DB += circle.db
DB += dbExample1.db
DB += dbExample2.db
DB += myexampleVersion.db
DB += dbSubExample.db
DB += user.substitutions
DB += modbus.db

# If <anyname>.db template is not named <anyname>*.template add
# <anyname>_TEMPLATE = <templatename>

include $(TOP)/configure/RULES
#----------------------------------------
#  ADD EXTRA GNUMAKE RULES BELOW HERE
```
## Configurer la communication modbus

On configure une connection modbus pour lire 2 mots de 16 bits (holding registers) sur un registre qu'on a appelé REG0 situé à l'adresse 1000.
- l'address de départ des registres vaut 1000
- le polling de lectures vaut 500 ms
- le type de données est entier non signé de 16 bits

Ajouter les lignes suivantes dans le fichier `topAutomateModbus/iocBoot/iocmodbusExample/st.cmd`.

```makefile
# drvModbusAsynConfigure(portName, => PLC_READ_REG0_L2_PORT
#                        tcpPortName, => PLC_TCP_PORT
#                        slaveAddress, => 1
#                        modbusFunction, => 3 (Read Holding Registers)
#                        modbusStartAddress, => 1000
#                        modbusLength, => 2
#                        dataType, => 0 (UINT16)
#                        pollMsec, => 500 ms
#                        plcType); => "PLC"
drvModbusAsynConfigure("PLC_READ_REG0_L2_PORT","PLC_TCP_PORT",1, 3, 1000, 2, 0, 500,"PLC")
```

## Chargement du fichier modbus.db dans l'IOC.
Ajouter les lignes suivantes dans le fichier `topAutomateModbus/iocBoot/iocmodbusExample/st.cmd`.
- C'est ici que les valeurs des macros VAL_COURANT_PORT, VAL_COURANT_OFFSET sont définies.
```makefile
dbLoadRecords("db/modbus.db", "user=epicslearner,VAL_COURANT_PORT=PLC_READ_REG0_L2_PORT,VAL_COURANT_OFFSET=0")
```

## Compilation de l'IOC
```shell
cd topAutomateModbus
make distclean
make
```
## Lancement de l'IOC
```shell
cd topAutomateModbus/iocBoot/iocmodbusExampleApp
./st.cmd
```
**NOTE :**
- vérifier avec la commande dbl que la PV `epicslearner:ValCourant` est bien présente.

## Vérification que la communication fonctionne bien
Nous utilisons l'outil modbusMechanic. C'est un logiciel développé en java qui permet de lancer un esclave modbus TCP. On le lance avec la commande suivante :
```shell
sudo java -jar ModbusMechanic.jar
```

![Alt text](modbusMechanicMaster.png)

Vous pouvez ensuite ouvrir un esclave TCP en utilisant le menu `Tools/Start Slave Simulator`. On prend un numéro d'esclave qui vaut 1.
![Alt text](modbusMechanicSlave1000.png)

Ici, j'ai configuré l'esclave n°1 pour changer la valeur du registre situé à l'adresse 1000.

On peut ensuite utiliser la commande camonitor pour vérifier que notre IOC voit les changements de valeur sur l'automate.
Si on change la valeur du registre 1000 alors on doit voir les changements dans le camonitor.

```shell
[epicslearner@rocky ModbusMechanic]$ camonitor epicslearner:ValCourant
epicslearner:ValCourant        2022-08-19 17:04:22.304495 10  
epicslearner:ValCourant        2022-08-19 17:04:24.868667 100  
epicslearner:ValCourant        2022-08-19 17:04:25.881599 1000  
epicslearner:ValCourant        2022-08-19 17:04:29.958527 1  
```


# Création d'une communication modbus pour écrire une valeur analogique sur un automate
On veut définir un record qu'on appellera consCourant pour écrire la consigne de courant (entier signé sur 32 bits) dans l'automate à l'adresse 2000.

A vous de jouer !

Pour vérifier que l'écriture modbus fonctionne, j'ai configuré l'esclave n°1 pour lire la valeur du registre situé à l'adresse 2000.
![Alt text](modbusMechanicSlave2000.png)

On pourra écrire une valeur dans la PV et voir que la valeur du registre modbus change.
```shell
[epicslearner@rocky ~]$ caput epicslearner:ConsCourant 2
Old : epicslearner:ConsCourant       1
New : epicslearner:ConsCourant       2
[epicslearner@rocky ~]$ 
```

# Création d'une communication modbus pour lire/écrire une valeur binaire sur un automate (records bi et bo)
On veut définir 2 records :
- `StatusVanne` qui permettra de lire la valeur binaire d'une électrovanne. Le registre à utiliser est un holding register se situant à l'addresse modbus 2002. Le bit à adresser est le bit n°0. On prendra comme convention 0 = ouvert et 1 = fermé.
- `CmdOuvrirVanne` qui permettra d'activer l'ouverture de l'électrovanne. Le registre est un holding register se situant à l'addresse modbus 2003. Le bit à addresser est le bit n°1.
- `CmdFermerVanne` qui permettra d'activer la fermeture de l'électrovanne. Le registre est un holding register se situant à l'addresse modbus 2003. Le bit à addresser est le bit n°2.

A vous de jouer !
Pour de l'aide, cf https://epics.anl.gov/modules/soft/asyn/R4-38/asynDriver.html.


# Création d'une communication modbus pour lire/écrire plusieurs valeurs binaires (records mbbi et mbbo)
Ces records permets de définir des états multi-bits. Par exemple, on peut imaginer qu'un automate renvoie un mot de 16 bits contenant des défauts, chaque bit correspondant à un défaut particulier. 
On veut définir un record `mbbi` avec 3 bits :
- bit 0 = defaut 1
- bit 1 = defaut 2
- bit 2 = defaut 3

Le registre à utiliser est un holding register se situant à l'addresse modbus 2004.

Aide : Cf https://epics.anl.gov/modules/soft/asyn/R4-38/asynDriver.html et https://epics.anl.gov/base/R7-0/6-docs/mbbiRecord.html
- Le bit 0 correspond à la valeur 1.
- Le bit 1 correspond à la valeur 2.
- Le bit 2 correspond à la valeur 4.


# Création d'un communication modbus pour lire/écrire une valeur flottante
Je donne ici l'exemple d'un record pour lire une valeur flottante sur 64 bits.

```c
record(ai, "$(user):ValFlottant") {
        field(SCAN, "I/O Intr")
        field(DESC, "Valeur flottante")
        field(DTYP, "asynFloat64")
        field(INP, "@asyn($(VAL_FLOTTANT_PORT), 0)FLOAT32_BE")
        field(PREC,  "3")
        field(EGU,  "A")
}
`

La configuration du port VAL_FLOTTANT_PORT dans le fichier `iocBoot/iocmodbusExample/st.cmd` est la suivante :
```c
drvModbusAsynConfigure("PLC_READ_SLAVE1_REG2010_L2_PORT","PLC_TCP_PORT", 1, 3, 2010, 4, 8, 500,"PLC")
```
Voici l'explication des paramètres.
```c
drvModbusAsynConfigure(portName, => PLC_WRITE_SLAVE1_REG2000_L2_PORT
                        tcpPortName, => PLC_TCP_PORT
                        slaveAddress, => 1
                        modbusFunction, => 3 (Read Holding Registers)
                        modbusStartAddress, => 2010
                        modbusLength, => 4
                        dataType, => 8 (FLOAT32_BE)
                        pollMsec, => 1 ms
                        plcType); => "PLC"
```


#!../../bin/linux-x86_64/modbusExample

#- You may have to change modbusExample to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/modbusExample.dbd"
modbusExample_registerRecordDeviceDriver pdbbase

# drvAsynIPPortConfigure("portName","hostInfo",priority,noAutoConnect,noProcessEos)
# The default priority is used
# the noAutoConnect flag is set to 0 so that asynManager will do normal automatic connection management. 
# The noProcessEos flag is set to 1 because Modbus over TCP does not require end-of-string processing. 
drvAsynIPPortConfigure("PLC_TCP_PORT","127.0.0.1:502",0,0,1)

# modbusInterposeConfig(tcpPortName, 
#                       linkType (0 for TCP),
#                       timeoutMsec,
#                       writeDelayMsec)
modbusInterposeConfig("PLC_TCP_PORT",0,300,2)

# drvModbusAsynConfigure(portName, => PLC_READ_SLAVE1_REG1000_L2_PORT
#                        tcpPortName, => PLC_TCP_PORT
#                        slaveAddress, => 1
#                        modbusFunction, => 3 (Read Holding Registers)
#                        modbusStartAddress, => 1000
#                        modbusLength, => 2
#                        dataType, => 0 (UINT16)
#                        pollMsec, => 500 ms
#                        plcType); => "PLC"
drvModbusAsynConfigure("PLC_READ_SLAVE1_REG1000_L2_PORT","PLC_TCP_PORT",1, 3, 1000, 2, 0, 500,"PLC")

# drvModbusAsynConfigure(portName, => PLC_WRITE_SLAVE1_REG2000_L2_PORT
#                        tcpPortName, => PLC_TCP_PORT
#                        slaveAddress, => 1
#                        modbusFunction, => 16 (Write Multiple Holding Registers)
#                        modbusStartAddress, => 2000
#                        modbusLength, => 2
#                        dataType, => 0 (UINT16)
#                        pollMsec, => 1 ms
#                        plcType); => "PLC"
drvModbusAsynConfigure("PLC_WRITE_SLAVE1_REG2000_L2_PORT","PLC_TCP_PORT",1, 16, 2000, 2, 0, 1,"PLC")

# configuration d'un bi
drvModbusAsynConfigure("PLC_READ_SLAVE1_REG2002_L1_PORT","PLC_TCP_PORT",1, 3, 2002, 1, 0, 500,"PLC")

# configuration d'un bo
drvModbusAsynConfigure("PLC_WRITE_SLAVE1_REG2003_L1_PORT","PLC_TCP_PORT", 1, 16, 2003, 1, 0, 500,"PLC")

# configuration d'un mbbi
drvModbusAsynConfigure("PLC_READ_SLAVE1_REG2004_L1_PORT","PLC_TCP_PORT",1, 3, 2004, 1, 0, 500,"PLC")

# configuration d'un flottant (dataType = 8 pour FLOAT32_BE)
drvModbusAsynConfigure("PLC_READ_SLAVE1_REG2010_L2_PORT","PLC_TCP_PORT", 1, 3, 2010, 4, 8, 500,"PLC")

## Load record instances
dbLoadTemplate "db/user.substitutions"
dbLoadRecords "db/modbusExampleVersion.db", "user=epicslearner"
dbLoadRecords "db/dbSubExample.db", "user=epicslearner"
dbLoadRecords("db/modbus.db", "user=epicslearner,VAL_COURANT_PORT=PLC_READ_SLAVE1_REG1000_L2_PORT,CONS_COURANT_PORT=PLC_WRITE_SLAVE1_REG2000_L2_PORT,VANNE_STATUS_PORT=PLC_READ_SLAVE1_REG2002_L1_PORT,VANNE_CMD_PORT=PLC_WRITE_SLAVE1_REG2003_L1_PORT,DEFAUTS_PORT=PLC_READ_SLAVE1_REG2004_L1_PORT,VAL_FLOTTANT_PORT=PLC_READ_SLAVE1_REG2010_L2_PORT")

#- Set this to see messages from mySub
#var mySubDebug 1

#- Run this to trace the stages of iocInit
#traceIocInit

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncExample, "user=epicslearner"
